<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>

<portlet:defineObjects/>

<div class="restaurantArea">
    <c:forEach var="table" items="${tables}">
        <div class="${table.name}#${table.peopleNumber}#${table.smoking}#end restaurant-table"
             style="top: ${table.positionY}px;
                     left: ${table.positionX}px;
                     width: ${table.width}px;
                     height: ${table.length}px;">
        </div>
    </c:forEach>
</div>
<div class="addTableMenu">
    <portlet:actionURL name="saveTable" var="saveTableAction"/>
    <form:form action="${saveTableAction}" modelAttribute="table">
        <h3><spring:message code="table.add"/></h3>
        <div class="formItem">
            <form:label for="name" cssClass="customLabel" cssErrorClass="error" path="name">
                <span><spring:message code="table.name"/></span>
                <span>
                    <form:input id="name" class="custom-input" htmlEscape="true" path="name"/>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="name"/></span>
        </div>

        <div class="formItem">
            <form:label for="length" cssClass="customLabel" cssErrorClass="error" path="length">
                <span><spring:message code="table.length"/></span>
                <span>
                    <form:input id="length" class="custom-input" htmlEscape="true" path="length"/>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="length"/></span>
        </div>

        <div class="formItem">
            <form:label for="width" cssClass="customLabel" cssErrorClass="error" path="width">
                <span><spring:message code="table.width"/></span>
                <span>
                    <form:input id="width" class="custom-input" htmlEscape="true" path="width"/>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="width"/></span>
        </div>

        <div class="formItem">
            <form:label for="positionX" cssClass="customLabel" cssErrorClass="error" path="positionX">
                <span><spring:message code="table.position.left"/></span>
                <span>
                    <form:input id="positionX" class="custom-input" htmlEscape="true" path="positionX"/>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="positionX"/></span>
        </div>

        <div class="formItem">
            <form:label for="positionY" cssClass="customLabel" cssErrorClass="error" path="positionY">
                <span><spring:message code="table.position.up"/></span>
                <span>
                    <form:input id="positionY" class="custom-input" htmlEscape="true" path="positionY"/>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="positionY"/></span>
        </div>

        <div class="formItem">
            <form:label for="peopleNumber" cssClass="customLabel" cssErrorClass="error" path="peopleNumber">
                <span><spring:message code="table.people.number"/></span>
                <span>
                    <form:input id="peopleNumber" class="custom-input" htmlEscape="true" path="peopleNumber"/>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="peopleNumber"/></span>
        </div>

        <spring:message code="table.select.option.smoking" var="option1"/>
        <spring:message code="table.select.option.no.smoking" var="option2"/>
        <div class="formItem">
            <form:label for="smoking" cssClass="customLabel" cssErrorClass="error" path="smoking">
                <span><spring:message code="table.area"/></span>
                <span>
                    <form:select id="smoking" htmlEscape="true" path="smoking">
                        <form:option value="Dla palących" label="${option1}"/>
                        <form:option value="Dla niepalących" label="${option2}" selected="selected"/>
                    </form:select>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="smoking"/></span>
        </div>

        <spring:message code="btn.add" var="btnAdd"/>
        <div>
            <input class="btn" type="submit" value="${btnAdd}"/>
        </div>
    </form:form>
</div>

