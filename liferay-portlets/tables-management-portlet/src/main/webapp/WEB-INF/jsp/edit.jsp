<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@taglib prefix="aui" uri="http://liferay.com/tld/aui" %>


<liferay-theme:defineObjects/>
<portlet:defineObjects/>
<div class="restaurantArea">
    <c:forEach var="table" items="${tables}">
        <div class="${table.id}#${table.name}#${table.peopleNumber}#${table.smoking}#end restaurant-table"
             style="top: ${table.positionY}px;
                    left: ${table.positionX}px;
                    width: ${table.width}px;
                     height: ${table.length}px;"
             onclick="editTable(this.className, this.style.top, this.style.left, this.style.width, this.style.height)">
        </div>
    </c:forEach>
</div>
<div class="addTableMenu">
    <portlet:actionURL name="updateTable" var="updateTableAction"/>
    <form:form action="${updateTableAction}" modelAttribute="table">
        <h3><spring:message code="table.update"/></h3>
        <div class="formItem">
            <form:input type="hidden" id="id" class="custom-input" htmlEscape="true" path="id"/>
            <form:label for="name" cssClass="customLabel" cssErrorClass="error" path="name">
                <span><spring:message code="table.name"/></span>
                <span>
                    <form:input id="name" class="custom-input" htmlEscape="true" path="name"/>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="name"/></span>
        </div>

        <div class="formItem">
            <form:label for="length" cssClass="customLabel" cssErrorClass="error" path="length">
                <span><spring:message code="table.length"/></span>
                <span>
                    <form:input id="length" class="custom-input" htmlEscape="true" path="length"/>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="length"/></span>
        </div>

        <div class="formItem">
            <form:label for="width" cssClass="customLabel" cssErrorClass="error" path="width">
                <span><spring:message code="table.width"/></span>
                <span>
                    <form:input id="width" class="custom-input" htmlEscape="true" path="width"/>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="width"/></span>
        </div>

        <div class="formItem">
            <form:label for="positionX" cssClass="customLabel" cssErrorClass="error" path="positionX">
                <span><spring:message code="table.position.left"/></span>
                <span>
                    <form:input id="positionX" class="custom-input" htmlEscape="true" path="positionX"/>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="positionX"/></span>
        </div>

        <div class="formItem">
            <form:label for="positionY" cssClass="customLabel" cssErrorClass="error" path="positionY">
                <span><spring:message code="table.position.up"/></span>
                <span>
                    <form:input id="positionY" class="custom-input" htmlEscape="true" path="positionY"/>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="positionY"/></span>
        </div>

        <div class="formItem">
            <form:label for="peopleNumber" cssClass="customLabel" cssErrorClass="error" path="peopleNumber">
                <span><spring:message code="table.people.number"/></span>
                <span>
                    <form:input id="peopleNumber" class="custom-input" htmlEscape="true" path="peopleNumber"/>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="peopleNumber"/></span>
        </div>

        <spring:message code="table.select.option.smoking" var="option1"/>
        <spring:message code="table.select.option.no.smoking" var="option2"/>
        <div class="formItem">
            <form:label for="smoking" cssClass="customLabel" cssErrorClass="error" path="smoking">
                <span><spring:message code="table.area"/></span>
                <span>
                    <form:select id="smoking" htmlEscape="true" path="smoking">
                        <form:option value="Dla palących" label="${option1}"/>
                        <form:option value="Dla niepalących" label="${option2}" selected="selected"/>
                    </form:select>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="smoking"/></span>
        </div>

        <portlet:renderURL var="updateTable">
            <portlet:param name="action" value="updateTable"/>
        </portlet:renderURL>

        <spring:message code="btn.update" var="btnUpdate"/>
        <div>
            <input ${disabled} id="updateBtn" class="btn" type="submit" value="${btnUpdate}"/>
        </div>

        <portlet:actionURL var="removeTable">
            <portlet:param name="action" value="removeTable"/>
        </portlet:actionURL>

        <div>
            <a ${disabled} id="deleteBtn"
                           href="${removeTable}&_tablesmanagementportlet_WAR_tablesmanagementportlet_id="
                           class="btn"><spring:message code="btn.remove"/></a>
        </div>
    </form:form>

    <portlet:renderURL var="addTable">
        <portlet:param name="action" value="addTable"/>
    </portlet:renderURL>
    <div>
        <a href="${addTable}" class="btn"><spring:message code="btn.add.new"/></a>
    </div>
</div>

