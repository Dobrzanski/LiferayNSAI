package pl.dmcs.nsai.portlets.dao;

import pl.dmcs.nsai.portlets.model.Table;

import java.util.List;

public interface TablesManagementDao {
    void saveTable(Table table);
    void updateTable(Table table);

    void removeTable(Table table);
    List<Table> getAllTables();
    Table findTableById(Integer tableId);
}
