package pl.dmcs.nsai.portlets.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.nsai.portlets.model.Table;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class TablesManagementDaoImpl implements TablesManagementDao {

    @PersistenceContext(unitName = "tablePU")
    private EntityManager em;

    @Override
    public void saveTable(Table table) {
        em.persist(table);
    }

    @Override
    public void updateTable(Table table) {
        em.merge(table);
    }

    @Override
    public void removeTable(Table table) {
        table = this.em.merge(table);
        this.em.remove(table);
    }

    @Override
    public List<Table> getAllTables() {
        Query query = em.createQuery("select e from " + Table.class.getName() + " e");
        return (List<Table>) query.getResultList();
    }

    @Override
    public Table findTableById(Integer tableId) {
        return em.find(Table.class, tableId);
    }
}
