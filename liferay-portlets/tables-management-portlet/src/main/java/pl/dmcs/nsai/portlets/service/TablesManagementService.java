package pl.dmcs.nsai.portlets.service;

import pl.dmcs.nsai.portlets.model.Table;

import java.util.List;

public interface TablesManagementService {
    void saveTable(Table table);
    void updateTable(Table table);
    void removeTable(Table table);
    List<Table> getAllTables();

    Table findTableById(Integer tableId);
}
