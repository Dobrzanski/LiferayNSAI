package pl.dmcs.nsai.portlets.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dmcs.nsai.portlets.dao.TablesManagementDao;
import pl.dmcs.nsai.portlets.model.Table;

import java.util.List;

@Service
public class TablesManagementServiceImpl implements TablesManagementService {

    @Autowired
    TablesManagementDao tablesManagementDao;

    @Override
    public void saveTable(Table table) {
        tablesManagementDao.saveTable(table);
    }

    @Override
    public void updateTable(Table table) {
        tablesManagementDao.updateTable(table);
    }

    @Override
    public void removeTable(Table table) {
        tablesManagementDao.removeTable(table);
    }

    @Override
    public List<Table> getAllTables() {
        return tablesManagementDao.getAllTables();
    }

    @Override
    public Table findTableById(Integer tableId) {
        return tablesManagementDao.findTableById(tableId);
    }
}
