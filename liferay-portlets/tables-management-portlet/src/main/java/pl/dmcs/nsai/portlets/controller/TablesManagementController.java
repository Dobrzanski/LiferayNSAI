package pl.dmcs.nsai.portlets.controller;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import pl.dmcs.nsai.portlets.model.Table;
import pl.dmcs.nsai.portlets.service.TablesManagementService;
import pl.dmcs.nsai.portlets.validator.TablesManagementValidator;

import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import java.util.List;

@Controller
@RequestMapping("VIEW")
public class TablesManagementController {

    private static final Log LOG = LogFactoryUtil.getLog(TablesManagementController.class);

    private static final String ID = "id";
    private static final String ACTION = "action";
    private static final String TABLE = "table";
    private static final String TABLE_LIST = "tables";
    private static final String ADD_TABLE = "addTable";
    private static final String SAVE_TABLE = "saveTable";
    private static final String UPDATE_TABLE = "updateTable";
    private static final String REMOVE_TABLE = "removeTable";
    private static final String ADD_VIEW = "add";
    private static final String EDIT_VIEW = "edit";
    private static final String DISABLED_PARAM = "disabled";

    @Autowired
    private TablesManagementService tablesManagementService;

    @Autowired
    private TablesManagementValidator tablesManagementValidator;

    @RenderMapping
    public String view(Model model, RenderRequest request) {

        if (!model.containsAttribute(TABLE)) {
            model.addAttribute(TABLE, new Table());
            model.addAttribute(DISABLED_PARAM, DISABLED_PARAM);
        }
        List<Table> tableList = tablesManagementService.getAllTables();
        model.addAttribute(TABLE_LIST, tableList);

        return EDIT_VIEW;
    }

    @RequestMapping(params = "action=" + ADD_TABLE)
    public String addView(Model model, RenderRequest request) {

        if (!model.containsAttribute(TABLE)) {
            model.addAttribute(TABLE, new Table());
        }
        List<Table> tableList = tablesManagementService.getAllTables();
        model.addAttribute(TABLE_LIST, tableList);

        return ADD_VIEW;
    }

    @ActionMapping(SAVE_TABLE)
    public void saveTable(@ModelAttribute(TABLE) Table table, BindingResult result,
                          PortletRequest request, ActionResponse response, Model model) {

        tablesManagementValidator.validate(table, result);
        if (!result.hasErrors()) {
            tablesManagementService.saveTable(table);
            response.setRenderParameter(ACTION, EDIT_VIEW);
        } else {
            response.setRenderParameter(ACTION, ADD_TABLE);
        }
    }

    @ActionMapping(UPDATE_TABLE)
    public void updateTable(@ModelAttribute(TABLE) Table table, BindingResult result,
                            PortletRequest request, ActionResponse response, Model model) {

        tablesManagementValidator.validate(table, result);
        if (!result.hasErrors()) {
            tablesManagementService.updateTable(table);
            response.setRenderParameter(ACTION, EDIT_VIEW);
        } else {
            response.setRenderParameter(ACTION, EDIT_VIEW);
        }
    }

    @ActionMapping(params = {"action=" + REMOVE_TABLE})
    public void removeTable(@RequestParam(ID) Integer tableId, PortletRequest request, ActionResponse response, Model model) {

        Table table = tablesManagementService.findTableById(tableId);
        tablesManagementService.removeTable(table);
        response.setRenderParameter(ACTION, EDIT_VIEW);
    }
}