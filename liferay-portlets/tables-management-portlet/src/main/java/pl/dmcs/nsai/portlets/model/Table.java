package pl.dmcs.nsai.portlets.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@javax.persistence.Table(name = "Restaurant_tables")
@Data
public class Table {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private Integer length;
    private Integer width;
    private Integer positionX;
    private Integer positionY;
    private Integer peopleNumber;
    private String smoking;
}
