package pl.dmcs.nsai.portlets.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.nsai.portlets.model.Reservation;
import pl.dmcs.nsai.portlets.model.Table;
import pl.dmcs.nsai.portlets.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class TablesReservationDaoImpl implements TablesReservationDao {

    @PersistenceContext(unitName = "reservationPU")
    private EntityManager em;

    @Override
    public boolean checkUserExist(String email) {
        StringBuilder queryString;
        queryString = new StringBuilder("select c from " + User.class.getName() + " c where c.email='" + email + "' ");
        Query query = em.createQuery(queryString.toString());
        List<User> users = query.getResultList();

        if (users.size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public List<Table> getAllTables() {
        Query query = em.createQuery("select e from " + Table.class.getName() + " e");
        return (List<Table>) query.getResultList();
    }

    @Override
    public Table findTableById(Integer tableId) {
        return em.find(Table.class, tableId);
    }

    @Override
    public Reservation findReservationById(Integer reservationId) {
        return em.find(Reservation.class, reservationId);
    }

    @Override
    public Reservation updateReservation(Reservation reservation) {
        return em.merge(reservation);
    }

    @Override
    public User findUserByEmail(String email) {
        StringBuilder queryString;
        queryString = new StringBuilder("select c from " + User.class.getName() + " c where c.email='" + email + "' ");
        Query query = em.createQuery(queryString.toString());
        return (User) query.getResultList().get(0);
    }

    @Override
    public void saveReservation(Reservation reservation, Integer tableId, User user) {

        Table table = findTableById(tableId);
        reservation.setTable(table);

        boolean flag = checkUserExist(user.getEmail());

        if (flag) {
            user = findUserByEmail(user.getEmail());
        }
        reservation.setUser(user);

        em.merge(reservation);
    }

    @Override
    public List<Reservation> getAllReservations() {
        Query query = em.createQuery("select e from " + Reservation.class.getName() + " e");
        return (List<Reservation>) query.getResultList();
    }

    @Override
    public List<Reservation> getReservationsByUser(User user) {
        StringBuilder queryString;
        queryString = new StringBuilder("select c from " + Reservation.class.getName() + " c where c.user='" + user.getId() + "' and c.active=1");
        Query query = em.createQuery(queryString.toString());
        return (List<Reservation>) query.getResultList();
    }
}
