package pl.dmcs.nsai.portlets.model;

import javax.activation.DataSource;
import java.io.*;

public class BufferedDataSource implements DataSource {

    private byte[] _data;
    private java.lang.String _name;

    public BufferedDataSource(byte[] data, String name) {
        _data = data;
        _name = name;
    }

    public String getContentType() {
        return "application/octet-stream";
    }

    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(_data);
    }

    public String getName() {
        return _name;
    }

    public OutputStream getOutputStream() throws IOException {
        OutputStream out = new ByteArrayOutputStream();
        out.write(_data);
        return out;
    }
}