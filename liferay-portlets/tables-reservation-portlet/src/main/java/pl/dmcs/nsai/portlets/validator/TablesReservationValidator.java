package pl.dmcs.nsai.portlets.validator;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import pl.dmcs.nsai.portlets.model.Table;

@Component
public class TablesReservationValidator implements Validator {

    private static final Log LOG = LogFactoryUtil.getLog(TablesReservationValidator.class);

    private static final String ERROR_EMPTY_INPUT_CODE = "error.empty.input";


    private static final String NAME_FIELD = "name";
    private static final String LENGTH_FIELD = "length";
    private static final String WIDTH_FIELD = "width";
    private static final String POSITION_X_FIELD = "positionX";
    private static final String POSITION_Y_FIELD = "positionY";
    private static final String PEOPLE_NUMBER_FIELD = "peopleNumber";

    @Override
    public boolean supports(Class<?> clazz) {
        return Table.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, NAME_FIELD, ERROR_EMPTY_INPUT_CODE);
        ValidationUtils.rejectIfEmpty(errors, LENGTH_FIELD, ERROR_EMPTY_INPUT_CODE);
        ValidationUtils.rejectIfEmpty(errors, WIDTH_FIELD, ERROR_EMPTY_INPUT_CODE);
        ValidationUtils.rejectIfEmpty(errors, POSITION_X_FIELD, ERROR_EMPTY_INPUT_CODE);
        ValidationUtils.rejectIfEmpty(errors, POSITION_Y_FIELD, ERROR_EMPTY_INPUT_CODE);
        ValidationUtils.rejectIfEmpty(errors, PEOPLE_NUMBER_FIELD, ERROR_EMPTY_INPUT_CODE);
    }
}
