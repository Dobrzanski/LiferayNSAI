package pl.dmcs.nsai.portlets.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@javax.persistence.Table(name = "Restaurant_reservations")
@Data
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    Date termStart;
    Date termEnd;
    Integer peopleNumber;

    @ManyToOne(cascade = CascadeType.ALL)
    User user;

    @OneToOne(cascade = CascadeType.ALL)
    Table table;

    Boolean active;
    Boolean status;
}
