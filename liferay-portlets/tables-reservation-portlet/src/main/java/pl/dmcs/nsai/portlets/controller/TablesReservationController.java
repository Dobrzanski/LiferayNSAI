package pl.dmcs.nsai.portlets.controller;

import com.itextpdf.text.DocumentException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import pl.dmcs.nsai.portlets.model.Date;
import pl.dmcs.nsai.portlets.model.Reservation;
import pl.dmcs.nsai.portlets.model.Table;
import pl.dmcs.nsai.portlets.service.TablesReservationService;
import pl.dmcs.nsai.portlets.validator.TablesReservationValidator;

import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("VIEW")
public class TablesReservationController {

    private static final Log LOG = LogFactoryUtil.getLog(TablesReservationController.class);

    private static final String ACTION = "action";
    private static final String INFO_MESSAGE = "message";
    private static final String TODAY = "today";
    private static final String USER = "user";
    private static final String TABLE_ID = "tableId";
    private static final String DATE_START = "dateStart";
    private static final String TIME_START = "timeStart";
    private static final String DATE_END = "dateEnd";
    private static final String TIME_END = "timeEnd";
    private static final String RESERVATION = "reservation";
    private static final String RESERVATIONS = "reservations";
    private static final String TABLE_LIST = "tables";
    private static final String CHECK_AVAILABLE_TABLES = "checkAvailableTables";
    private static final String SAVE_RESERVATION = "saveReservation";
    private static final String SHOW_PDF = "showPdf";
    private static final String SHOW_MENU = "showMenu";
    private static final String SHOW_CONFIRMATION = "showConfirmation";
    private static final String SHOW_RESERVATIONS = "showReservations";
    private static final String REMOVE_RESERVATION = "removeReservation";
    private static final String TABLE_CHOICE_VIEW = "tableChoice";
    private static final String MENU_VIEW = "menu";
    private static final String TERM_CHOICE_VIEW = "termChoice";
    private static final String RESERVATIONS_VIEW = "reservations";
    private static final String RESERVATION_CONFIRMATION_VIEW = "reservationConfirmation";
    private static final String DATE_PATTERN = "dd/MM/yyyy HH:mm";

    @Autowired
    private TablesReservationService tablesReservationService;

    @Autowired
    private TablesReservationValidator tablesReservationValidator;

    @RenderMapping
    public String view(Model model, RenderRequest request) {

        Reservation reservation = new Reservation();
        Date todayDate = new Date();
        Calendar today = Calendar.getInstance();

        todayDate.setDay(today.get(Calendar.DAY_OF_MONTH));
        todayDate.setMonth(today.get(Calendar.MONTH));
        todayDate.setYear(today.get(Calendar.YEAR));
        todayDate.setHour(today.get(Calendar.HOUR_OF_DAY));
        todayDate.setMinute(today.get(Calendar.MINUTE));

        model.addAttribute(RESERVATION, reservation);
        model.addAttribute(TODAY, todayDate);


        List<Reservation> reservations = tablesReservationService.getAllReservations();

        String test = "Test";

        return TERM_CHOICE_VIEW;
    }

    @RequestMapping(params = "action=" + CHECK_AVAILABLE_TABLES)
    public String checkAvailableTables(@RequestParam(value = DATE_START, required = false) String dateStart,
                                       @RequestParam(value = TIME_START, required = false) String timeStart,
                                       @RequestParam(value = DATE_END, required = false) String dateEnd,
                                       @RequestParam(value = TIME_END, required = false) String timeEnd,
                                       @ModelAttribute(RESERVATION) Reservation reservation, Model model,
                                       RenderRequest request) throws ParseException {

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        User user = themeDisplay.getUser();

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
        java.util.Date termStart = sdf.parse(dateStart + " " + timeStart);
        java.util.Date termEnd = sdf.parse(dateEnd + " " + timeEnd);
        reservation.setTermStart(termStart);
        reservation.setTermEnd(termEnd);

        List<Table> availableTables = new ArrayList<>();
        List<Integer> excludedTables = new ArrayList<>();
        List<Table> tableList = tablesReservationService.getAllTables();
        List<Reservation> reservations = tablesReservationService.getAllReservations();

        for (Reservation r : reservations) {
            if (termStart.after(r.getTermStart()) && termEnd.before(r.getTermEnd())
                    || termStart.before(r.getTermStart()) && termEnd.after(r.getTermStart()) && termEnd.before(r.getTermEnd())
                    || termStart.after(r.getTermStart()) && termStart.before(r.getTermEnd()) && termEnd.after(r.getTermEnd())
                    || termStart.before(r.getTermStart()) && termEnd.after(r.getTermEnd())) {

                excludedTables.add(r.getTable().getId());
            }
        }

        Set<Integer> excludedTablesUnique = new LinkedHashSet<>(excludedTables);

        for (Table table : tableList) {
            if (!excludedTablesUnique.contains(table.getId())) {
                availableTables.add(table);
            }
        }

        if (availableTables.size() == 0) {
            model.addAttribute(INFO_MESSAGE, "Brak wolnych stolików w podanym terminie");
        }
        model.addAttribute(USER, user);
        model.addAttribute(TABLE_LIST, availableTables);
        model.addAttribute(RESERVATION, reservation);
        return TABLE_CHOICE_VIEW;
    }

    @ActionMapping(SAVE_RESERVATION)
    public void saveReservation(@RequestParam(value = TABLE_ID, required = false) Integer tableId,
                                @ModelAttribute(RESERVATION) Reservation reservation, BindingResult result,
                                PortletRequest request, ActionResponse response, Model model) throws SystemException, DocumentException {

        Table table = tablesReservationService.findTableById(tableId);
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        User liferayUser = themeDisplay.getUser();
        pl.dmcs.nsai.portlets.model.User user = new pl.dmcs.nsai.portlets.model.User();
        user.setName(liferayUser.getFirstName());
        user.setSurname(liferayUser.getLastName());
        user.setEmail(liferayUser.getEmailAddress());

        if (liferayUser.getPhones().size() > 0) {
            user.setPhoneNumber(liferayUser.getPhones().get(0).getNumber());
        }
        reservation.setStatus(false);
        reservation.setActive(true);
        tablesReservationService.saveReservation(reservation, tableId, user);

        byte[] bytePdf = tablesReservationService.generatePdf(table.getName(), reservation.getTermStart().toString(),
                reservation.getTermEnd().toString(), reservation.getPeopleNumber());
        tablesReservationService.sendMail(user.getEmail(), "Potwierdzenie dokonania rezerwacji", user.getName() + " " + user.getSurname(), bytePdf);
        response.setRenderParameter(ACTION, SHOW_CONFIRMATION);
    }

    @RequestMapping(params = "action=" + SHOW_CONFIRMATION)
    public String showConfirmation(Model model, RenderRequest request) {
        return RESERVATION_CONFIRMATION_VIEW;
    }

    @ActionMapping(params = {"action=" + SHOW_PDF})
    public void showPdf(@RequestParam(required = false) String tableName, @RequestParam(required = false) String termStart,
                        @RequestParam(required = false) String termEnd, @RequestParam(required = false) Integer peopleNumber,
                        PortletRequest request, ActionResponse actionResponse) throws DocumentException {

        HttpServletResponse response = PortalUtil.getHttpServletResponse(actionResponse);
        byte[] bytePdf = tablesReservationService.generatePdf(tableName, termStart, termEnd, peopleNumber);

        try {
            OutputStream o = response.getOutputStream();
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "inline; filename=Raport.pdf");
            response.flushBuffer();

            o.write(bytePdf);
            o.flush();
            o.close();
        } catch (IOException e) {

        }
    }

    @RequestMapping(params = "action=" + SHOW_RESERVATIONS)
    public String showReservations(Model model, RenderRequest request) {

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        User liferayUser = themeDisplay.getUser();
        pl.dmcs.nsai.portlets.model.User currentUser = tablesReservationService.findUserByEmail(liferayUser.getEmailAddress());
        List<Reservation> reservations = tablesReservationService.getReservationsByUser(currentUser);

        model.addAttribute(RESERVATIONS, reservations);
        return RESERVATIONS_VIEW;
    }

    @ActionMapping(params = {"action=" + REMOVE_RESERVATION})
    public void removeReservation(@RequestParam(required = false) Integer reservationId,
                                  PortletRequest request, ActionResponse response) {

        Reservation reservation = tablesReservationService.findReservationById(reservationId);
        reservation.setActive(false);
        tablesReservationService.updateReservation(reservation);

        response.setRenderParameter(ACTION, SHOW_RESERVATIONS);
    }

    @RequestMapping(params = "action=" + SHOW_MENU)
    public String showMenu(Model model, RenderRequest request) {

        JSONParser parser = new JSONParser();

        try {

            Object obj = parser.parse(new FileReader("/home/dawid8493/workspace/liferay6.2/custom-modules/files/menu.json"));

            JSONObject jsonObject = (JSONObject) obj;

            model.addAttribute("test", jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return MENU_VIEW;
    }
}