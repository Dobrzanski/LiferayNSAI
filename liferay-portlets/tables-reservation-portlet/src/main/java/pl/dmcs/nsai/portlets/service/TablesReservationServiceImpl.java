package pl.dmcs.nsai.portlets.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dmcs.nsai.portlets.dao.TablesReservationDao;
import pl.dmcs.nsai.portlets.model.BufferedDataSource;
import pl.dmcs.nsai.portlets.model.Reservation;
import pl.dmcs.nsai.portlets.model.Table;
import pl.dmcs.nsai.portlets.model.User;

import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Service
public class TablesReservationServiceImpl implements TablesReservationService {

    @Autowired
    TablesReservationDao tablesReservationDao;

    @Override
    public boolean checkUserExist(String email) {
        return tablesReservationDao.checkUserExist(email);
    }

    @Override
    public List<Table> getAllTables() {
        return tablesReservationDao.getAllTables();
    }

    @Override
    public Table findTableById(Integer tableId) {
        return tablesReservationDao.findTableById(tableId);
    }

    @Override
    public Reservation findReservationById(Integer reservationId) {
        return tablesReservationDao.findReservationById(reservationId);
    }

    @Override
    public Reservation updateReservation(Reservation reservation) {
        return tablesReservationDao.updateReservation(reservation);
    }

    @Override
    public User findUserByEmail(String email) {
        return tablesReservationDao.findUserByEmail(email);
    }

    @Override
    public void saveReservation(Reservation reservation, Integer tableId, User user) {
        tablesReservationDao.saveReservation(reservation, tableId, user);
    }

    @Override
    public List<Reservation> getAllReservations() {
        return tablesReservationDao.getAllReservations();
    }

    @Override
    public byte[] generatePdf(String tableName, String termStart, String termEnd, Integer peopleNumber) throws DocumentException {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        byte[] bytePDF = new byte[0];

        try {
            Document document = new Document();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            PdfWriter.getInstance(document, stream);

            document.open();

            BaseFont helvetica = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
            Font helvetica16 = new Font(helvetica, 16, Font.BOLD);
            Font helvetica12 = new Font(helvetica, 12);

            Paragraph paragraph = new Paragraph(new Phrase("Łódź, "
                    + dateFormat.format(new Date()).toString() + "r.", helvetica12));
            paragraph.setAlignment(Element.ALIGN_RIGHT);
            document.add(paragraph);

            document.add(new Paragraph(""));
            document.addTitle("Potwierdzenie złożenia rezerwacji");

            Paragraph paragraph2 = new Paragraph("Potwierdzenie złożenia rezerwacji", helvetica16);
            paragraph2.add(new Paragraph(" "));
            paragraph2.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph2);

            PdfPTable pdfTable = new PdfPTable(4);

            PdfPCell cell1 = new PdfPCell(new Phrase("Nazwa stolika", helvetica12));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell1);

            cell1 = new PdfPCell(new Phrase("Od", helvetica12));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell1);

            cell1 = new PdfPCell(new Phrase("Do", helvetica12));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell1);

            cell1 = new PdfPCell(new Phrase("Liczba osób", helvetica12));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell1);

            pdfTable.setHeaderRows(1);

            pdfTable.addCell(tableName);
            pdfTable.addCell(termStart);
            pdfTable.addCell(termEnd);
            pdfTable.addCell(String.valueOf(peopleNumber));

            document.add(pdfTable);
            document.close();

            bytePDF = stream.toByteArray();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bytePDF;
    }

    @Override
    public void sendMail(String to, String subject, String user, byte[] bytePdf) {
        final String username = "spoldzielniajee@gmail.com";
        final String password = "jeejeejee";

        Properties props = new Properties();
        props.put("mail.smtp.auth", true);
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("spoldzielniajee@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("dawid8493@gmail.com"));
            message.setSubject(subject);

            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent("Witaj " + user + ". Dziękujemy za złożenie rezerwacji. W załączniku znajduje się " +
                    "potwierdzenie.", "text/html;charset=utf-8");

            MimeBodyPart attachPart = new MimeBodyPart();
            Multipart multipart = new MimeMultipart();
            BufferedDataSource bds = new BufferedDataSource(bytePdf, "Potwierdzenie.pdf");
            attachPart.setDataHandler(new DataHandler(bds));
            attachPart.setFileName(bds.getName());

            multipart.addBodyPart(messageBodyPart);
            multipart.addBodyPart(attachPart);
            message.setContent(multipart);

            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Reservation> getReservationsByUser(User user) {
        return tablesReservationDao.getReservationsByUser(user);
    }
}
