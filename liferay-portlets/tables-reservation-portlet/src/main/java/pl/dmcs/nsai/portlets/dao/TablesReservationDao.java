package pl.dmcs.nsai.portlets.dao;

import pl.dmcs.nsai.portlets.model.Reservation;
import pl.dmcs.nsai.portlets.model.Table;
import pl.dmcs.nsai.portlets.model.User;

import java.util.List;

public interface TablesReservationDao {
    boolean checkUserExist(String email);

    List<Table> getAllTables();

    Table findTableById(Integer tableId);

    Reservation findReservationById(Integer reservationId);

    Reservation updateReservation(Reservation reservation);

    User findUserByEmail(String email);

    void saveReservation(Reservation reservation, Integer tableId, User user);

    List<Reservation> getAllReservations();

    List<Reservation> getReservationsByUser(User user);
}
