package pl.dmcs.nsai.portlets.model;

import lombok.Data;

@Data
public class Date {
    int day;
    int month;
    int year;
    int hour;
    int minute;
}