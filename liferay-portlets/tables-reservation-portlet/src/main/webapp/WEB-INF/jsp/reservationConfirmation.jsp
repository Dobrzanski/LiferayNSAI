<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui" %>
<%@taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme" %>
<%@taglib prefix="aui" uri="http://liferay.com/tld/aui" %>


<liferay-theme:defineObjects/>
<portlet:defineObjects/>

<h4>Pomyślnie zarezerwowano stolik.</h4>
<span>Na adres e-mail podany przy rejestracji została wysłana wiadomość ze szczegółami dokonanej rezerwacji.</span></br>

<portlet:actionURL var="showPdf">
    <portlet:param name="action" value="showPdf"/>
    <portlet:param name="tableName" value="${reservation.table.name}"/>
    <portlet:param name="termStart" value="${reservation.termStart}"/>
    <portlet:param name="termEnd" value="${reservation.termEnd}"/>
    <portlet:param name="peopleNumber" value="${reservation.peopleNumber}"/>
</portlet:actionURL>

<a class="print-btn" href="${showPdf}"><i class="icon-print"></i>Wydrukuj potwierdzenie</a>
