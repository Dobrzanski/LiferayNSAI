<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui" %>
<%@taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme" %>
<%@taglib prefix="aui" uri="http://liferay.com/tld/aui" %>


<liferay-theme:defineObjects/>
<portlet:defineObjects/>
<h3>Wybierz stolik, a następnie wypełnij formularz rezerwacji</h3>
<div class="restaurantArea">
    <c:forEach var="table" items="${tables}">
        <div class="${table.id}#${table.name}#${table.peopleNumber}#${table.smoking}#end restaurant-table"
             style="top: ${table.positionY}px;
                     left: ${table.positionX}px;
                     width: ${table.width}px;
                     height: ${table.length}px;"
             onclick="reserveTable(this.className, this.style.top, this.style.left, this.style.width, this.style.height)">
        </div>
    </c:forEach>
</div>
<div class="table-info">
    <portlet:actionURL name="saveReservation" var="saveReservationAction"/>
    <form:form action="${saveReservationAction}" modelAttribute="reservation">
        <h4>Wybrany stolik:</h4>
        <p id="table-name">Nazwa stolika: </p>
        <p id="term-start">Od: </p>
        <p id="term-end">Do: </p>
        <p id="people-number">Liczba osób: </p>
        <form:input id="peopleNumber" class="custom-input" htmlEscape="true" path="peopleNumber" type="hidden"/>
        <form:input id="termStart" class="custom-input" htmlEscape="true" path="termStart" type="hidden"/>
        <form:input id="termEnd" class="custom-input" htmlEscape="true" path="termEnd" type="hidden"/>
        <input id="tableId" class="custom-input" name="tableId" htmlEscape="true" type="hidden"/>
        <div>
            <input disabled class="btn" id="reserveBtn" type="submit" value="Rezerwuj"/>
        </div>
    </form:form>
    <p>${message}</p>
</div>

