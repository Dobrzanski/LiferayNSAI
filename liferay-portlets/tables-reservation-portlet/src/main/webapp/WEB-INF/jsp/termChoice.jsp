<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui" %>
<%@taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme" %>

<liferay-theme:defineObjects/>
<portlet:defineObjects/>

<div class="term-choice">
    <h3>Wybierz termin rezerwacji</h3>

    <portlet:renderURL var="checkTables">
        <portlet:param name="action" value="checkAvailableTables"/>
    </portlet:renderURL>
    <form:form action="${checkTables}" modelAttribute="reservation">
        <div class="formItem">
                <span>Od</span>
                <span>
                    <liferay-ui:input-date
                            dayParam="day-start"
                            dayValue="${today.day}"
                            disabled="<%= false %>"
                            monthParam="month-start"
                            monthValue="${today.month}"
                            yearParam="year-start"
                            yearValue="${today.year}"
                            name="dateStart"
                    />
                </span>
            <span><form:errors element="span" cssClass="form-error"/></span>
        </div>

        <div class="formItem">
                <span>
                    <liferay-ui:input-time
                            amPmParam="timeAmPm"
                            amPmValue="0"
                            hourParam="hour-start"
                            hourValue="${today.hour}"
                            minuteParam="minute-start"
                            minuteValue="${today.minute}"
                            name="timeStart"
                    />
                </span>
            <span><form:errors element="span" cssClass="form-error"/></span>
        </div>

        <div class="formItem">
                <span>Do</span>
                <span>
                    <liferay-ui:input-date
                            dayParam="day-end"
                            dayValue="${today.day}"
                            disabled="<%= false %>"
                            monthParam="month-end"
                            monthValue="${today.month}"
                            yearParam="year-end"
                            yearValue="${today.year}"
                            name="dateEnd"
                    />
                </span>
            <span><form:errors element="span" cssClass="form-error"/></span>
        </div>

        <div class="formItem">
                <span>
                    <liferay-ui:input-time
                            amPmParam="timeAmPm"
                            amPmValue="0"
                            hourParam="hour-end"
                            hourValue="${today.hour}"
                            minuteParam="minute-end"
                            minuteValue="${today.minute}"
                            name="timeEnd"
                    />
                </span>
            <span><form:errors element="span" cssClass="form-error"/></span>
        </div>

        <div class="formItem">
            <form:label for="peopleNumber" cssClass="customLabel" cssErrorClass="error" path="peopleNumber">
                <span>Liczba osób</span>
                <span>
                    <form:select id="peopleNumber" htmlEscape="true" path="peopleNumber">
                        <c:forEach var="i" begin="1" end="5">
                            <form:option value="${i}" label="${i}"/>
                        </c:forEach>
                    </form:select>
                </span>
            </form:label>
            <span><form:errors element="span" cssClass="form-error" path="peopleNumber"/></span>
        </div>

        <div>
            <input class="btn" type="submit" value="Szukaj stolików"/>
        </div>

    </form:form>

    <portlet:renderURL var="showReservations">
        <portlet:param name="action" value="showReservations"/>
    </portlet:renderURL>

    <a class="management-btn" href="${showReservations}"><i class="icon-print"></i>Moje rezerwacje</a>

    <portlet:renderURL var="showMenu">
        <portlet:param name="action" value="showMenu"/>
    </portlet:renderURL>

    <a class="management-btn" href="${showMenu}"><i class="icon-list"></i>Menu restauracji</a>
</div>