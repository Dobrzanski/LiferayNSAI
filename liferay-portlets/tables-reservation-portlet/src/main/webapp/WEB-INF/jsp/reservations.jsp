<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui" %>
<%@taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme" %>

<liferay-theme:defineObjects/>
<portlet:defineObjects/>

<h4>Twoje rezerwacje</h4>
<table class="reservations-list">
    <thead>
    <th>Nazwa stolika</th>
    <th>Od</th>
    <th>Do</th>
    <th>Liczba osób</th>
    <th>Akcje</th>
    </thead>
    <tbody>
    <portlet:actionURL var="removeReservation">
        <portlet:param name="action" value="removeReservation"/>
    </portlet:actionURL>

    <c:forEach items="${reservations}" var="reservation">
        <tr>
            <td>${reservation.table.name}</td>
            <td>${reservation.termStart}</td>
            <td>${reservation.termEnd}</td>
            <td>${reservation.peopleNumber}</td>
            <td>
                <a href="${removeReservation}&_tablesreservationportlet_WAR_tablesreservationportlet_reservationId=${reservation.id}">Anuluj
                    rezerwację</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>