function reserveTable(className, top, left, width, height) {
    /*var _top = top.replace("px", "");
    var _left = left.replace("px", "");
    var _width = width.replace("px", "");
     var _height = height.replace("px", "");*/
    var _className = className.split("#");

    var paragraphName = document.getElementById("table-name").innerHTML;
    paragraphName = paragraphName.substring(0, 15) + _className[1];
    document.getElementById("table-name").innerHTML = paragraphName;

    var reserveBtn = document.getElementById("reserveBtn");
    reserveBtn.removeAttribute("disabled");

    document.getElementById("tableId").value = _className[0];
}

window.onload = function () {
    document.getElementById("termStart").readOnly = true;
    document.getElementById("termEnd").readOnly = true;

    var peopleNumber = document.getElementById("peopleNumber").value;
    var paragraphPeopleNumber = document.getElementById("people-number").innerHTML;
    document.getElementById("people-number").innerHTML = paragraphPeopleNumber.substr(0, 13) + peopleNumber;

    var termStart = document.getElementById("termStart").value;
    var paragraphTermStart = document.getElementById("term-start").innerHTML;
    document.getElementById("term-start").innerHTML = paragraphTermStart.substr(0, 4) + termStart;

    var termEnd = document.getElementById("termEnd").value;
    var paragraphTermEnd = document.getElementById("term-end").innerHTML;
    document.getElementById("term-end").innerHTML = paragraphTermEnd.substr(0, 4) + termEnd;
};

