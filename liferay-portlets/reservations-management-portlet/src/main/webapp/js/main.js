function editTable(className, top, left, width, height) {
    var _top = top.replace("px", "");
    var _left = left.replace("px", "");
    var _width = width.replace("px", "");
    var _height = height.replace("px", "");
    var _className = className.split("#");

    document.getElementById("name").value = _className[1];
    document.getElementById("length").value = _height;
    document.getElementById("width").value = _width;
    document.getElementById("positionX").value = _left;
    document.getElementById("positionY").value = _top;
    document.getElementById("peopleNumber").value = _className[2];
    document.getElementById("smoking").value = _className[3];
    document.getElementById("id").value = _className[0];

    setValueToRemoveTable(_className[0]);
}

function setValueToRemoveTable(tableId) {
    var link;
    var tableToRemove = tableId;
    var anchor = document.getElementById("deleteBtn");
    var href = anchor.getAttribute("href");

    link = href.substring(0, href.indexOf('portlet_id='));
    link = link + "portlet_id=" + tableToRemove;

    anchor.href = link;
    anchor.removeAttribute("disabled");
    document.getElementById("updateBtn").removeAttribute("disabled");
}

window.onload = function () {
    var link;
    var tableToRemove = document.getElementById("id").value;
    var anchor = document.getElementById("deleteBtn");
    var href = anchor.getAttribute("href");
    link = href.substring(0, href.indexOf('portlet_id='));
    link = link + "portlet_id=" + tableToRemove;
    anchor.href = link;
};