<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui" %>
<%@taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme" %>
<%@taglib prefix="aui" uri="http://liferay.com/tld/aui" %>


<liferay-theme:defineObjects/>
<portlet:defineObjects/>
<h4>Lista rezerwacji</h4>
<liferay-ui:search-container searchContainer="${reservationsContainer}" emptyResultsMessage="empty"
                             delta="${reservationsContainer.delta}">
    <liferay-ui:search-container-results results="${reservationsContainer.results}"
                                         total="${reservationsContainer.total}"/>
    <liferay-ui:search-container-row indexVar="index" className="pl.dmcs.nsai.portlets.model.Reservation"
                                     modelVar="reservation" rowVar="myRow">

        <portlet:actionURL var="realizeUrl">
            <portlet:param name="action" value="realizeReservation"/>
            <portlet:param name="reservationId" value="${reservation.id}"/>
        </portlet:actionURL>

        <liferay-ui:search-container-column-text name="Stolik" orderable="${true}" orderableProperty="${'table.name'}">
            <span>${reservation.table.name}</span>
        </liferay-ui:search-container-column-text>

        <liferay-ui:search-container-column-text name="Od" orderable="${true}" orderableProperty="${'termStart'}">
            <span>${reservation.termStart}</span>
        </liferay-ui:search-container-column-text>

        <liferay-ui:search-container-column-text name="Do" orderable="${true}" orderableProperty="${'termEnd'}">
            <span>${reservation.termEnd}</span>
        </liferay-ui:search-container-column-text>

        <liferay-ui:search-container-column-text name="Ilość osób" orderable="${true}"
                                                 orderableProperty="${'peopleNumber'}">
            <span>${reservation.peopleNumber}</span>
        </liferay-ui:search-container-column-text>

        <liferay-ui:search-container-column-text name="Dane rezerwującego" orderable="${true}"
                                                 orderableProperty="${'user.name'}">
            <span>${reservation.user.name} ${reservation.user.surname} ${reservation.user.email} ${reservation.user.phoneNumber}</span>
        </liferay-ui:search-container-column-text>

        <liferay-ui:search-container-column-text name="Status" orderable="${false}">
            <c:choose>
                <c:when test="${reservation.status == false}">
                    <span>Nie zrealizowano</span>
                </c:when>
                <c:otherwise>
                    <span>Zrealizowano</span>
                </c:otherwise>
            </c:choose>
        </liferay-ui:search-container-column-text>

        <liferay-ui:search-container-column-text name="Akcje" orderable="${false}">
            <c:choose>
                <c:when test="${reservation.status == false}">
                    <a href="${realizeUrl}">Zrealizuj</a>
                </c:when>
            </c:choose>
        </liferay-ui:search-container-column-text>
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator paginate="true"/>
</liferay-ui:search-container>
