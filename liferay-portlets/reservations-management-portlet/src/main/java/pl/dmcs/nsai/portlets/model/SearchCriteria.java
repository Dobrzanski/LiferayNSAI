package pl.dmcs.nsai.portlets.model;

import lombok.Data;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

@Data
public class SearchCriteria implements Serializable {

    private static final long serialVersionUID = 8625528845888953146L;
    private String orderColName;
    private String orderType;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
