package pl.dmcs.nsai.portlets.controller;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import pl.dmcs.nsai.portlets.container.ReservationsContainer;
import pl.dmcs.nsai.portlets.model.Reservation;
import pl.dmcs.nsai.portlets.model.SearchCriteria;
import pl.dmcs.nsai.portlets.service.ReservationsManagementService;
import pl.dmcs.nsai.portlets.validator.ReservationsManagementValidator;

import javax.portlet.*;
import java.util.List;

@Controller
@RequestMapping("VIEW")
public class ReservationsManagementController {

    private static final Log LOG = LogFactoryUtil.getLog(ReservationsManagementController.class);

    private static final String ACTION = "action";
    private static final String RESERVATIONS_CONTAINER = "reservationsContainer";
    private static final String SEARCH_CRITERIA = "searchCriteria";
    private static final String RESERVATIONS_VIEW = "reservations";
    private static final String SHOW_RESERVATIONS = "showReservations";
    private static final String REALIZE_RESERVATION = "realizeReservation";

    @Autowired
    private ReservationsManagementService reservationsManagementService;

    @Autowired
    private ReservationsManagementValidator reservationsManagementValidator;

    @RenderMapping
    public String view(Model model, RenderResponse response, RenderRequest request, PortletPreferences preferences) throws PortletModeException, SystemException, PortalException {

        SearchCriteria searchCriteria = new SearchCriteria();
        PortletURL iteratorURL = response.createRenderURL();

        String portletResource = ParamUtil.getString(request, "portletResource");

        if (Validator.isNotNull(portletResource)) {
            preferences = PortletPreferencesFactoryUtil.getPortletSetup(request, portletResource);
        }

        Integer rowsOnPage = Integer.valueOf(preferences.getValue("rowsOnPage", ""));

        try {
            iteratorURL.setPortletMode(new PortletMode("VIEW"));
            iteratorURL.setParameter(ACTION, SHOW_RESERVATIONS);

            ReservationsContainer reservationsContainer = new ReservationsContainer(request, iteratorURL, rowsOnPage);
            if (reservationsContainer.getOrderByCol() != null && reservationsContainer.getOrderByCol() != "") {
                searchCriteria.setOrderColName(reservationsContainer.getOrderByCol());
            }
            if (reservationsContainer.getOrderByType() != null && reservationsContainer.getOrderByType() != "") {
                searchCriteria.setOrderType(reservationsContainer.getOrderByType());
            }
            List<Reservation> reservations = reservationsManagementService.getFilteredReservations(searchCriteria);
            reservationsContainer.setResults(ListUtil.subList(reservations, reservationsContainer.getStart(), reservationsContainer.getEnd()));
            reservationsContainer.setTotal(reservations.size());

            model.addAttribute(RESERVATIONS_CONTAINER, reservationsContainer);
            model.addAttribute(SEARCH_CRITERIA, searchCriteria);
        } catch (Exception e) {
            LOG.error(e);
        }

        return RESERVATIONS_VIEW;
    }


    @RequestMapping(params = "action=" + SHOW_RESERVATIONS)
    public String test(@ModelAttribute(SEARCH_CRITERIA) SearchCriteria searchCriteria, BindingResult result,
                       Model model, RenderResponse response, RenderRequest request, PortletPreferences preferences) throws SystemException, PortalException {

        String portletResource = ParamUtil.getString(request, "portletResource");

        if (Validator.isNotNull(portletResource)) {
            preferences = PortletPreferencesFactoryUtil.getPortletSetup(request, portletResource);
        }

        Integer rowsOnPage = Integer.valueOf(preferences.getValue("rowsOnPage", ""));
        PortletURL iteratorURL = response.createRenderURL();

        try {
            iteratorURL.setPortletMode(new PortletMode("VIEW"));
            iteratorURL.setParameter(ACTION, SHOW_RESERVATIONS);

            ReservationsContainer reservationsContainer = new ReservationsContainer(request, iteratorURL, rowsOnPage);
            if (reservationsContainer.getOrderByCol() != null && reservationsContainer.getOrderByCol() != "") {
                searchCriteria.setOrderColName(reservationsContainer.getOrderByCol());
            }
            if (reservationsContainer.getOrderByType() != null && reservationsContainer.getOrderByType() != "") {
                searchCriteria.setOrderType(reservationsContainer.getOrderByType());
            }
            List<Reservation> reservations = reservationsManagementService.getFilteredReservations(searchCriteria);
            reservationsContainer.setResults(ListUtil.subList(reservations, reservationsContainer.getStart(), reservationsContainer.getEnd()));
            reservationsContainer.setTotal(reservations.size());

            model.addAttribute(RESERVATIONS_CONTAINER, reservationsContainer);
            model.addAttribute(SEARCH_CRITERIA, searchCriteria);
        } catch (Exception e) {
            LOG.error(e);
        }

        return RESERVATIONS_VIEW;
    }

    @ActionMapping(params = {"action=" + REALIZE_RESERVATION})
    public void realizeReservation(@RequestParam(required = false) Integer reservationId, PortletRequest request, ActionResponse response) {

        Reservation reservation = reservationsManagementService.findReservationById(reservationId);
        reservation.setStatus(true);
        reservationsManagementService.updateReservation(reservation);

        response.setRenderParameter(ACTION, SHOW_RESERVATIONS);
    }
}