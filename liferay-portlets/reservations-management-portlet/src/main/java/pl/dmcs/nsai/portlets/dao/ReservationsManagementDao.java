package pl.dmcs.nsai.portlets.dao;

import pl.dmcs.nsai.portlets.model.Reservation;
import pl.dmcs.nsai.portlets.model.SearchCriteria;
import pl.dmcs.nsai.portlets.model.User;

import java.util.List;

public interface ReservationsManagementDao {
    User findUserByEmail(String email);

    Reservation findReservationById(Integer reservationId);

    void updateReservation(Reservation reservation);
    List<Reservation> getFilteredReservations(SearchCriteria searchCriteria) throws Exception;
}
