package pl.dmcs.nsai.portlets.dao;

import com.liferay.portal.kernel.util.StringPool;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.nsai.portlets.model.Reservation;
import pl.dmcs.nsai.portlets.model.SearchCriteria;
import pl.dmcs.nsai.portlets.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class ReservationsManagementDaoImpl implements ReservationsManagementDao {

    @PersistenceContext(unitName = "tablePU")
    private EntityManager em;

    @Override
    public User findUserByEmail(String email) {
        StringBuilder queryString;
        queryString = new StringBuilder("select c from " + User.class.getName() + " c where c.email='" + email + "' ");
        Query query = em.createQuery(queryString.toString());
        return (User) query.getResultList().get(0);
    }

    @Override
    public Reservation findReservationById(Integer reservationId) {
        return em.find(Reservation.class, reservationId);
    }

    @Override
    public void updateReservation(Reservation reservation) {
        em.merge(reservation);
    }

    @Override
    public List<Reservation> getFilteredReservations(SearchCriteria searchCriteria) throws Exception {
        List<Reservation> result = null;
        StringBuilder queryString = new StringBuilder("select c from " + Reservation.class.getName() + " c ");


        if (StringUtils.isNotBlank(searchCriteria.getOrderColName())) {
            queryString.append(" ORDER BY ");
            queryString.append(searchCriteria.getOrderColName());
            if (StringUtils.isNotBlank(searchCriteria.getOrderType())) {
                queryString.append(StringPool.SPACE);
                queryString.append(searchCriteria.getOrderType());
            }
        }

        Query query = em.createQuery(queryString.toString());

        result = query.getResultList();

        return result;
    }
}
