package pl.dmcs.nsai.portlets.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dmcs.nsai.portlets.dao.ReservationsManagementDao;
import pl.dmcs.nsai.portlets.model.Reservation;
import pl.dmcs.nsai.portlets.model.SearchCriteria;
import pl.dmcs.nsai.portlets.model.User;

import java.util.List;

@Service
public class ReservationsManagementServiceImpl implements ReservationsManagementService {

    @Autowired
    ReservationsManagementDao reservationsManagementDao;

    @Override
    public User findUserByEmail(String email) {
        return reservationsManagementDao.findUserByEmail(email);
    }

    @Override
    public Reservation findReservationById(Integer reservationId) {
        return reservationsManagementDao.findReservationById(reservationId);
    }

    @Override
    public void updateReservation(Reservation reservation) {
        reservationsManagementDao.updateReservation(reservation);
    }

    @Override
    public List<Reservation> getFilteredReservations(SearchCriteria searchCriteria) throws Exception {
        return reservationsManagementDao.getFilteredReservations(searchCriteria);
    }
}
