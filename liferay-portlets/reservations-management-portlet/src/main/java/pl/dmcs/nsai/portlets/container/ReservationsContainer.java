package pl.dmcs.nsai.portlets.container;

import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.dao.search.SearchContainer;
import com.liferay.portal.kernel.util.ParamUtil;
import org.apache.commons.lang.StringUtils;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.WindowStateException;
import java.util.ArrayList;

public class ReservationsContainer extends SearchContainer {
    public ReservationsContainer(PortletRequest request, PortletURL iteratorURL, int delta) throws WindowStateException {

        super(request, new DisplayTerms(request), new DisplayTerms(request), DEFAULT_CUR_PARAM, delta, iteratorURL, new ArrayList<String>(1), null);

        setOrderByColParam(DEFAULT_ORDER_BY_COL_PARAM);
        setOrderByTypeParam(DEFAULT_ORDER_BY_TYPE_PARAM);

        String requestParamOrderByColumn = ParamUtil.getString(request, getOrderByColParam());

        if (StringUtils.isBlank(requestParamOrderByColumn)) {
            setOrderByCol("");
        } else {
            setOrderByCol(requestParamOrderByColumn);
        }

        String requestParamOrderByType = ParamUtil.getString(request, getOrderByTypeParam());
        if (StringUtils.isBlank(requestParamOrderByType)) {
            setOrderByType("");
        } else {
            setOrderByType(requestParamOrderByType);
        }
    }
}
