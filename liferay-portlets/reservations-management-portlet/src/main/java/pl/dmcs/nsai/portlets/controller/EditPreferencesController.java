package pl.dmcs.nsai.portlets.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.*;
import java.io.IOException;

@Controller
@RequestMapping("EDIT")
public class EditPreferencesController {
    private static final Logger LOG = LoggerFactory.getLogger(EditPreferencesController.class);

    private static final String CONFIGURATION = "configuration";
    private static final String ROWS_ON_PAGE = "rowsOnPage";

    @RenderMapping
    public String showPreferences(Model model, PortletPreferences portletPreferences) throws PortletModeException {
        model.addAttribute("attribute", portletPreferences.getValue("prefVal", "default"));
        return CONFIGURATION;
    }

    @ActionMapping(params = "action=save")
    public void savePreferences(@RequestParam(ROWS_ON_PAGE) String myparam, PortletPreferences portletPreferences, ActionResponse response)
            throws ReadOnlyException, ValidatorException, IOException, PortletModeException {
        if (myparam != null && !myparam.equals("")) {
            portletPreferences.reset(ROWS_ON_PAGE);
            portletPreferences.setValue(ROWS_ON_PAGE, myparam);
        }
        portletPreferences.store();
    }
}
